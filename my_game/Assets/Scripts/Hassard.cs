﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hassard : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collisionData)
    {
        Collider2D objectWeCollideWith = collisionData.collider;
            
        PlayerHealth Player = objectWeCollideWith.GetComponent<PlayerHealth>(); 
    
        if (Player != null)
        {
            Player.kill();
        }
    }
}
