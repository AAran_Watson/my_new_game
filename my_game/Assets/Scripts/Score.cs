﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{

    public Text ScoreDisplay;
    public bool shouldReset = false;

    private static int ScoreValue = 0;
    
    void Start()
    {
        if (shouldReset == true)
        {
            ScoreValue = 0;

            
        }

        ScoreDisplay.text = ScoreValue.ToString();
    }
    
    public void AddScore(int toAdd)
    {
        ScoreValue = ScoreValue + toAdd;

        ScoreDisplay.text = ScoreValue.ToString();    
    }
}
