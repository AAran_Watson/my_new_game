﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public string GameOverScreen;
    
    public void kill()
    {
        Destroy(gameObject);

        SceneManager.LoadScene(GameOverScreen);
    }
}
