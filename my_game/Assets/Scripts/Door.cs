﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Door : MonoBehaviour
{
    public string levelToLoad;

    private void OnTriggerEnter2D(Collider2D othercollider)
    {
        Debug.Log("Object hit the door!");



        if (othercollider.tag == "Player") 
        {
            SceneManager.LoadScene(levelToLoad);
        }
    
                
     }
}
