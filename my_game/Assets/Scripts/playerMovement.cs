﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{


    public float movementForce;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Update()
    {
        // get our rigidbody that we'll need to find the physics information
        Rigidbody2D ourRigidbody = GetComponent<Rigidbody2D>();

        // find out from the rigidbody what our current horizontal and vertical speeds are
        float currentSpeedH = ourRigidbody.velocity.x;
        float currentSpeedV = ourRigidbody.velocity.y;

        //get the animator component that we will be using for setting our animation
        Animator ourAnimator= GetComponent<Animator>();

        //tell our animator what the speeds are
        ourAnimator.SetFloat("speedH", currentSpeedH);
        ourAnimator.SetFloat("speedV", currentSpeedV);
    }

    public void MoveUp()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.up* movementForce);
    }

    public void MoveLeft()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.left* movementForce);
    }

    public void MoveDown()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.down* movementForce);
    }

    public void MoveRight()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.right* movementForce);
    }
}
